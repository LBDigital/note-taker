package coursework;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;

public class AllCourses extends CommonCode{
    private ArrayList<String> courses = new ArrayList<>();
    private HashMap courseworks = new HashMap();
    private String course = "";

    AllCourses(){
        readAllCourses();
    }


    // Accessor (getter) methods
    
    /**
     * read all courses from file into ArrayList
     */
    private void readAllCourses(){
        ArrayList<String> readCourses = new ArrayList<>();

        readCourses = readTextFile(appDir + "/Courses/Courses.txt");


        // if courses file not found
        if (!readCourses.isEmpty() && "File not found".equals(readCourses.get(0))){
            System.out.println("Courses file not found, creating...");

            // create courses folder
            new File(appDir + "/Courses").mkdir();
            // create courses file
            String path = appDir + "/Courses/Courses.txt";
            try {
                writeTextFile(path, new ArrayList<String>()); // send blank ArrayList
            } catch (IOException ex){
                System.err.println("Error creating Courses.txt file at " + path);
            }

        }else{ // courses file found
            // read in courses from file to arraylist
            for (String crs : readCourses){
                // add course to courses arrayList
                courses.add(crs);
                // create coursework overview screen for course
                createCourseworkOverviewScreen(crs);
            }
            
            // write courses to ensure all courses have a directory
            writeAllCourses();
        }

        if (!courses.isEmpty()){
            // update currently chosen courses
            course = courses.get(0);
        }else{
            // active course is blank
            course = "";
        }

    }

    /**
     * Get currently chosen course
     * @return
     */
    public String getCourse(){
        return course;
    }

    /**
     * Get all courses
     * @return
     */
    public ArrayList<String> getCourses(){
        return courses;
    }
    
    public void showCourseworkScreen(){
        // show coursework overview screen for active course
        CourseworkOverview cwScreen = (CourseworkOverview) courseworks.get(this.getCourse());
        cwScreen.setVisible(true);
    }






    // Mutator (setter) methods

    /**
     * Write all courses form ArrayList to file
     */
    private void writeAllCourses(){
        // write courses from ArrayList to 'Courses.txt' file
        String path = appDir + "/Courses/Courses.txt";
        try{
            writeTextFile(path, courses);
        }catch(IOException ex){
            System.out.println("Failed to write courses to " + path);
        }
        
        // ensure all courses have their own directory
        for (String crs : courses){
            new File(appDir + "/Courses/" + crs).mkdir();
        }
    }

    /**
     * Add a course to available courses list
     * @param newCourse
     */
    public void addCourse(String newCourse){
        if (courses.contains(newCourse)){
            // course already exists
            String responseMsg = "Course '" + newCourse + "' already exists!";
            System.err.println(responseMsg);
            JOptionPane.showMessageDialog(null, responseMsg, "Add Course", JOptionPane.ERROR_MESSAGE);
            
        }else if(newCourse == null || newCourse.equals("")){
            // no course name given
            String responseMsg = "No Course name given!";
            System.err.println(responseMsg);
            JOptionPane.showMessageDialog(null, responseMsg, "Add Course", JOptionPane.ERROR_MESSAGE);
        }else{
            // add new course to courses list
            courses.add(newCourse);
            // save updated courses list to file & create directory for course
            writeAllCourses();
            // create coursework overview screen for new course
            createCourseworkOverviewScreen(newCourse);
            // set new course to be active course
            this.setCourse(newCourse);
        }
    }

    /**
     * Delete a course from available courses list
     * @param delCourse
     */
    public void deleteCourse(String delCourse){
        if (!courses.contains(delCourse)){
            String responseMsg = "Course '" + delCourse + "' doesn't exist!";
            JOptionPane.showMessageDialog(null, responseMsg, "Delete Course", JOptionPane.ERROR_MESSAGE);
        }else{
            // course exists, so delete it from courses list
            courses.remove(delCourse);
            // clear course directory so it can be deleted
            File dir = new File(appDir + "/Courses/" + delCourse);
            File[] listFiles = dir.listFiles();
            for (File file: listFiles){
                file.delete();
            }
            // delete course directory
            dir.delete();

            // save updated courses list to file
            writeAllCourses();
            // update active course if it was deleted
            if (course.equals(delCourse)){
                // if courses are available
                if (!courses.isEmpty()){
                    course = courses.get(0);
                }else{
                    // no courses, so active course blank
                    course = "";
                }
            }
        }
    }


    public void amendCourse(String target, String newTitle){
        if (!courses.contains(target)){
            String responseMsg = "Course '" + target + "' doesn't exist!";
            JOptionPane.showMessageDialog(null, responseMsg, "Amend Course", JOptionPane.ERROR_MESSAGE);
        }else{
            // course exists so amend its name in courses list
            int index = courses.indexOf(target);
            courses.set(index, newTitle);
            // save updated courses list to file
            writeAllCourses();
            
            // change courses directory name
            File currentDir = new File(appDir + "/Courses/" + target);
            File newDir = new File(appDir + "/Courses/" + newTitle);
            boolean isFileRenamed = currentDir.renameTo(newDir);
            if(!isFileRenamed) {
                System.err.println("Error renaming the file");
            }
            
            // update active course if it was target
            if (course.equals(target)){
                course = newTitle;
            }
        }
    }


    /**
     * Set currently chosen course
     * @param newCourse
     */
    public void setCourse(String newCourse){
        if (newCourse == null || newCourse.equals("")){
            System.out.println("Error Setting Course: no course name given.");
        }else{
            course = newCourse;
        }
    }
    
    
    
    public void createCourseworkOverviewScreen(String crs){
        // create coursework item for this course
        CourseworkItem cwItem = new CourseworkItem(crs);
        // generate coursework overview screen for this item
        CourseworkOverview cwScreen = new CourseworkOverview(cwItem);
        // add coursework overview screen to HashMap (dictionary)
        courseworks.put(crs, cwScreen);
    }

}