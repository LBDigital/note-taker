package coursework;

import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JOptionPane;

public class InputSentence{
    ArrayList<String> sentence = new ArrayList<String>();

    /**
     * Mutator method to set the sentence
     */
    public void setSentence(){
        String input;

        input = JOptionPane.showInputDialog(null, "Please enter your sentence");
        if ( !(input.equals("") || input==null) ){
            String[] tmp = input.split(" ");
            sentence = new ArrayList(Arrays.asList(tmp));
        }
    }

    /**
     * Accessor method for retrieving saved sentence
     * @return
     */
    public ArrayList<String> getSentence(){
        return sentence;
    }

}
