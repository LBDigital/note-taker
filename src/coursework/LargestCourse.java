/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author lucasbowers
 */
public class LargestCourse {
    
    public LargestCourse(){}
    
    
    
    public String getLargestCourse(ArrayList<Note> notes){
        HashMap counts = new HashMap();
        String largestCourse = getLargestCourse(notes, counts, 0);
        
        return largestCourse;
    }
    
    public String getLargestCourse(ArrayList<Note> notes, HashMap counts, int counter){
        
        if (counter == notes.size()-1){
            String largestCourse = "";
            int highestCount = 0;
            
            Iterator it = counts.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                System.out.println(pair.getKey() + " = " + pair.getValue());
                it.remove(); // avoids a ConcurrentModificationException
                
                int thisCount = (int) pair.getValue();
                
                if ( thisCount > highestCount ){
                    largestCourse = (String) pair.getKey();
                    highestCount = thisCount;
                }
                
            }
            return largestCourse;

        }else{
            
            Note n = notes.get(counter);
            String course = n.getCourse();
            
            int newCount = 1;
            if (counts.containsKey(course)){
                newCount += ((int) counts.get(course));
            }
            counts.put(course, newCount);
            
            return getLargestCourse(notes, counts, counter+1);
            
        }
    }
    
}
