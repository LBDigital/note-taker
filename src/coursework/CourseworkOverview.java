/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

/**
 *
 * @author lucasbowers
 */
public class CourseworkOverview extends JFrame implements ActionListener, KeyListener {
//    private CommonCode cc = new CommonCode();
    
    private CourseworkItem coursework;
    
    
    private JComboBox reqList = new JComboBox();
    private Boolean updatingReqList = false;
    
    private JTextArea txtNewReq = new JTextArea();
    private JTextArea txtDisplayReqs = new JTextArea();
    
    private JLabel pageTitle = new JLabel();
    
    
    
    CourseworkOverview(CourseworkItem cw){
        model(cw);
        view();
        controller();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if ("ExitCourseworkOverview".equals(ae.getActionCommand())){
            // hide this coursework overview window
            this.setVisible(false);
//            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }
        
        if ("NewRequirement".equals(ae.getActionCommand())){
            // create a new requirement
            String newReq;

            newReq = JOptionPane.showInputDialog(null, "Please enter the new requirement title...");
            //Entered requirement title isn't blank
            if (!newReq.equals("")){
                coursework.addRequirement(newReq);
                //Update the dropdown menu
                updateReqList();
            }
        }
        
        if ("NewRequirementNote".equals(ae.getActionCommand())){
            String reqNote = txtNewReq.getText();
            txtNewReq.setText("");
            coursework.addRequirementNote(reqNote);
            addAllReqNotes();
        }
        
        if ("ChangeRequirement".equals(ae.getActionCommand())){
            if (!updatingReqList){ // if dropdown not in process of being updated
                String newReq;

                newReq = (String) reqList.getSelectedItem();
                coursework.setActiveRequirement(newReq);
                //Update page title to show selected course
                updatePageTitle();
                //Show all notes for new course
                addAllReqNotes();
            }
        }
        
        if ("ExportRequirements".equals(ae.getActionCommand())){
            coursework.exportRequirements();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    private void model(CourseworkItem cw){
        coursework = cw;
        
        if (coursework.getRequirements().isEmpty()){
            coursework.addRequirement("Example Requirement");
        }
    }
    
    
    
    
    private void view(){
        Font fnt = new Font("Georgia",Font.PLAIN,24);

        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setTitle("Coursework Overview");

        // navbar
        JMenuBar menuBar = new JMenuBar();
        // 'Requirements' navbar dropdown
        JMenu reqmenu = new JMenu();
        
        reqmenu = new JMenu("Requirement");
        reqmenu.setToolTipText("Requirements Tasks");
        reqmenu.setFont(fnt);
        // add items to 'Requirement' dropdown
        reqmenu.add(makeMenuItem("New Requirement",
                "NewRequirement",
                "Add a new requirement for this modules coursework.",
                fnt
        ));
        reqmenu.add(makeMenuItem("Export Requirements",
                "ExportRequirements",
                "Export all requirements and notes for this coursework.",
                fnt
        ));
        menuBar.add(reqmenu);
        
        menuBar.add(makeMenuItem("Exit Coursework Overview for " + coursework.getCourseworkCourse(), 
                "ExitCourseworkOverview", 
                "Exit Coursework Overview for " + coursework.getCourseworkCourse(), 
                fnt
        ));

        // add requirements to JComboBox (reqList)
        reqList.setFont(fnt);
        reqList.setMaximumSize(reqList.getPreferredSize());
        reqList.setActionCommand("ChangeRequirement");
        reqList.addActionListener(this);
        // add courses to menubar
        menuBar.add(reqList);
        // update the dropdown to add all requirements
        updateReqList();
        this.setJMenuBar(menuBar);
        
        
        JToolBar toolBar = new JToolBar();
        // Setting up the button bar
        JButton button = null;
        // imageName, actionCmd, tooltipText, altText
        button = makeButton("Create", "NewRequirementNote", "Add a new note for requirement", "New");
        toolBar.add(button);
        toolBar.addSeparator();
        button = makeButton("exit", "ExitCourseworkOverview", "Exit Coursework Overview for " + coursework.getCourseworkCourse(), "Exit");
        toolBar.add(button);
        
        add(toolBar, BorderLayout.NORTH);
        
        

        // create West Panel
        JPanel pnlWest = new JPanel();
        pnlWest.setLayout(new BoxLayout(pnlWest, BoxLayout.Y_AXIS));
        //pnlWest.setPreferredSize(new Dimension(400,400));
        pnlWest.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        // add txtNewReq to west panel
        txtNewReq.setFont(fnt);
        pnlWest.add(txtNewReq);

        // add 'Add Requirement' button to West Panel
        JButton btnAddReq = new JButton("Add Requirement");
        btnAddReq.setActionCommand("NewRequirement");
        btnAddReq.addActionListener(this);
        pnlWest.add(btnAddReq);

        // add west panel to scene
        add(pnlWest, BorderLayout.WEST);

        
        // create Center Panel
        JPanel cen = new JPanel();
        cen.setLayout(new BoxLayout(cen, BoxLayout.Y_AXIS));
        cen.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        //add course title to notes
        updatePageTitle();
        Font fntTitle =new Font("Helvetica",Font.PLAIN,24);
        pageTitle.setFont(fntTitle);
        cen.add(pageTitle);

        // container to show requirement notes
        txtDisplayReqs.setFont(fnt);
        cen.add(txtDisplayReqs);

        add(cen, BorderLayout.CENTER);

//        setVisible(true);

    }
    
    
    private void controller(){
        addAllReqNotes();
    }
    
    
    
    
    private void addAllReqNotes(){
        txtDisplayReqs.removeAll();
        txtDisplayReqs.revalidate();
        txtDisplayReqs.repaint();

        ArrayList<String> reqNotes = coursework.getActiveRequirement().getRequirementNotes();

        String txtReqNotes = "";

        for (String reqNote : reqNotes){

            txtReqNotes += reqNote + "\n";
        }

        txtDisplayReqs.setText(txtReqNotes);
    }
    
    
    
    /**
     * Update the reqList JComboBox
     */
    public void updateReqList(){
        // so "ChangeRequirement" action event doesn't run
        updatingReqList = true;
        
        // clear dropdown
        reqList.removeAllItems();
        
        // get requirements for coursework item
        ArrayList<CourseworkRequirement> reqs = coursework.getRequirements();
        
        if ((!reqs.isEmpty())){ // if atleast 1 requirement
            //add each requirement to dropdown
            for (CourseworkRequirement req : reqs){
                // get title of loop-requirement
                String reqTitle = req.getTitle();
                reqList.addItem(reqTitle);
            }
            updatingReqList = false;
        
            // set selected course to active course
            reqList.setSelectedItem(coursework.getActiveRequirement().getTitle());
        }
    }
    
    
    /**
     * update the center panel title with active requirement
     */
    private void updatePageTitle(){
        System.out.println(coursework.getActiveRequirement().getTitle());
        pageTitle.setText("<html><body>"
                + "<h2>Requirement: <span style='font-size: 18px;'>"
                + coursework.getActiveRequirement().getTitle()
                + "</span></h2>"
                + "</body></html>"
        );
    }
    
    
    
    protected JMenuItem makeMenuItem (
            String txt,
            String actionCommand,
            String toolTipText,
            Font fnt) {

        JMenuItem mnuItem = new JMenuItem();

        mnuItem.setText(txt);
        mnuItem.setActionCommand(actionCommand);
        mnuItem.setToolTipText(toolTipText);
        mnuItem.setFont(fnt);

        mnuItem.addActionListener(this);

        return mnuItem;
    }

    protected JButton makeButton(
            String imageName,
            String actionCommand,
            String toolTipText,
            String altText) {

        //Create and initialize the button
        JButton button = new JButton();
        button.setToolTipText(toolTipText);
        button.setActionCommand(actionCommand);
        button.addActionListener(this);

        //Look for the image
        String imgLocation = System.getProperty("user.dir")
                + "/icons/"
                + imageName
                + ".png";

        File fyle = new File(imgLocation);
        if (fyle.exists() && !fyle.isDirectory()) {
            //Image found
            Icon img;
            img = new ImageIcon(imgLocation);
            button.setIcon(img);
        } else {
            //image not found
            button.setText(altText);
            System.err.println("Resource not found: " + imgLocation);
        }

        return button;

    }
    
    
}
