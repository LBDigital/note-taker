/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import java.util.ArrayList;
import javax.swing.JOptionPane;

public class SearchWord {

    /**
     * method to search for a word in given sentence 'sentence'
     * @param sentence
     */
    public void searchForWord(ArrayList<String> sentence){
        String searchTerm;
        Boolean found;
        String responseMsg;

        searchTerm = JOptionPane.showInputDialog(null, "What term do you want to search for?");
        found = false;
        for (String term : sentence){
            if (term.equals(searchTerm)){
                found = true;
            }
        }

        if (found){
            responseMsg = "Word found in sentence!";
        }else{
            responseMsg = "Word not found!";
        }
        JOptionPane.showMessageDialog(null, responseMsg, "Search Word", JOptionPane.WARNING_MESSAGE);

    }
}
