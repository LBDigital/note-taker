package coursework;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class AllNotes extends CommonCode{
    private AllCourses allCourses = new AllCourses();
    public ArrayList<Note> allNotes = new ArrayList<>();
    private int nextID = 0;
//    public int max = 0;

    AllNotes(){
        readAllNotes();
    }

    /**
     * Read all notes in from file
     */
    protected void readAllNotes(){
        ArrayList<String> readNotes = new ArrayList<>();

        readNotes = readTextFile(appDir + "/Notes.txt");

        if (!readNotes.isEmpty() && "File not found".equals(readNotes.get(0))){
            System.out.println("Notes file not found, creating...");

            // create notes file
            String path = appDir + "/Notes.txt";
            try {
                writeTextFile(path, new ArrayList<String>()); // send blank ArrayList
            } catch (IOException ex){
                System.err.println("Error creating Notes.txt file at " + path);
            }

        }else{
            allNotes.clear();
            nextID = 0;
            for (String str : readNotes){ //for each line of Notes.txt
                // split each line by tab character
                String[] tmp = str.split("\t");
                if (tmp.length == 4){// all needed data is there
                    int noteID;
                    String noteContent = "";

                    noteID = Integer.parseInt(tmp[0]);
                    String noteCourse = tmp[1];
                    String noteDate = tmp[2];
                    
                    try{
                        noteContent = URLDecoder.decode(tmp[3], "UTF-8");
                    } catch (UnsupportedEncodingException ex) {
                        Logger.getLogger(AllNotes.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    Note n = new Note(noteID, noteCourse, noteDate, noteContent);

                    allNotes.add(n);
                    nextID = noteID +1;
                }

            }

        }
    }

    protected void addNote(String course, String note){
        if (!course.equals("") && !note.equals("")){
            int id = getNextID();
            // create new Note instance
            Note n = new Note(id, course, null, note);
            // add note instance to ArrayList of all notes
            allNotes.add(n);
            // update save file
            writeAllNotes();
        }

    }

    private int getNextID(){
        int id = nextID;
        nextID++;
        return id;
    }

    /**
     * Get all notes
     * @return
     */
    public ArrayList<Note> getAllNotes(){
        return allNotes;
    }

    /**
     * Get all notes for given course 'c'
     * @param c
     * @return
     */
    public ArrayList<Note> getNotesForCourse(String c){
        ArrayList<Note> notesForCourse = new ArrayList<>();

        for (Note n : allNotes){
            if (c.equals(n.getCourse())){
                notesForCourse.add(n);
            }
        }
        return notesForCourse;
    }


    /**
     * Write all notes back to file
     */
    protected void writeAllNotes(){
        String path = appDir + "/Notes.txt";
        ArrayList<String> writeNote = new ArrayList<>();

        for (Note n : allNotes){
            String tmp = n.getNoteID() + "\t";
            tmp += n.getCourse() + "\t";
            tmp += n.getDate() + "\t";            

            // notes url encoded to support multi lines & special chars without breaking storage file formatting
            try {
                String noteTxt = URLEncoder.encode(n.getNote(), "UTF-8");
                tmp += noteTxt + "\t";
                writeNote.add(tmp);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(AllNotes.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }

        try{
            writeTextFile(path, writeNote);
        } catch(IOException ex){
            System.out.println("Problem! " + path);
        }
    }
    
    
    public String searchAllNotesByKeyword(String noteList, String s, ArrayList<Note> notesForCourse) {

        for (Note n : notesForCourse){

            if (n.getNote().contains(s)){
                noteList += n.getNote() + "\n";
            }
        }
        return noteList;
    }
    
}
