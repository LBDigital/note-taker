/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 *
 * @author lucasbowers
 */
public class CourseworkRequirement extends CommonCode{
    
    private String title = "";
    private ArrayList<String> reqNotes = new ArrayList<>();
    
    // param is the path for the requirement file
    CourseworkRequirement(String t, ArrayList<String> n){
        title = t;
        reqNotes = n;
    }
    
    
    protected String getTitle(){
        return title;
    }
    
    protected ArrayList<String> getRequirementNotes(){
        return reqNotes;
    }
    
    protected void addRequirementNote(String reqNote){
        if (!reqNote.equals("")){
            reqNotes.add(reqNote);
        }
    }
    
}
