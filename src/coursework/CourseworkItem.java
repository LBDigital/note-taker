/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lucasbowers
 */
public class CourseworkItem extends CommonCode{
    private String course = "";
    private ArrayList<CourseworkRequirement> requirements = new ArrayList<>();
    private int activeReqIndex = -1;
    
    CourseworkItem(String crs){
        course = crs;
        
        readAllRequirements();
    }
    
    /**
     * Go through each requirement, loading its notes
     */
    private void readAllRequirements(){
        System.out.println("readAllRequirements()");
        // clear any existing requirements
        requirements.clear();
        
        // if course has a "Coursework" directory
        String dirPath = appDir + "/Courses/" + course + "/Coursework";
        File dir = new File(dirPath);
        File[] reqFiles = dir.listFiles(); // reqFiles -> requirementFiles
        if (reqFiles != null){ // course has a "Coursework" directory
            System.out.println("AAAAAAA");
            for (File reqFile : reqFiles){ // for each requirement file...
                System.out.println("BBBBB");
                // read req file
                ArrayList<String> readFile = readTextFile(reqFile.getPath());
                // tmp store requirement data
                String reqTitle = "";
                ArrayList<String> reqNotes = new ArrayList<>();
                //for each line of req file
                for (String line : readFile){
                    if (reqTitle.equals("")){ // first line is the title of the req
                        reqTitle = line;
                    }else{
                        try {
                            // add requirement note to ArrayList
                            line = URLDecoder.decode(line, "UTF-8");
                            reqNotes.add(line);
                        } catch (UnsupportedEncodingException ex) {
                            Logger.getLogger(CourseworkItem.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                // instantiate CourseworkRequirement class and add to ArrayList
                requirements.add(new CourseworkRequirement(reqTitle, reqNotes));
            }
            
            if (!requirements.isEmpty()){
                activeReqIndex = 0;
            }else{
                activeReqIndex = -1;
            }
        }else{
            System.out.println("Coursework directory not found, creating...");

            // create Coursework directory
            new File(dirPath).mkdir();
        }
    }
    
    
    protected ArrayList<CourseworkRequirement> getRequirements(){
        return requirements;
    }
    
    protected String getCourseworkCourse(){
        return course;
    }
    
    
    protected CourseworkRequirement getActiveRequirement(){
        if (activeReqIndex < 0){
            return null;
        }else{
            return requirements.get(activeReqIndex);
        }
    }
    
    protected void addRequirementNote(String reqNote){
        requirements.get(activeReqIndex).addRequirementNote(reqNote);
        writeAllRequirements();
    }
    
    protected void setActiveRequirement(String reqTitle){
        int index = 0;
        for (CourseworkRequirement req : requirements){
            if (req.getTitle().equals(reqTitle)){
                activeReqIndex = index;
            }
            index++;
        }
    }
    
    
    protected void addRequirement(String reqTitle){
        CourseworkRequirement newReq = new CourseworkRequirement(reqTitle, new ArrayList<>());
        requirements.add(newReq);
        activeReqIndex = requirements.size()-1;
        writeAllRequirements();
    }
    
    private void writeAllRequirements(){
        // delete existing coursework requirements
        
        // if course has a "Coursework" directory
        String dirPath = appDir + "/Courses/" + course + "/Coursework";
        File dir = new File(dirPath);
        File[] reqFiles = dir.listFiles(); // reqFiles -> requirementFiles
        if (reqFiles != null){ // course has a "Coursework" directory
            for (File reqFile : reqFiles){ // for each requirement file...
                // delete file
                reqFile.delete();
            }
        }else{
            System.out.println("Coursework directory not found, creating...");

            // create Coursework directory
            new File(dirPath).mkdir();
        }
        
        
        // empty Coursework directory exists, write all requirements...
        int reqId = 0;
        for (CourseworkRequirement req : requirements){ // for each requirement
            ArrayList<String> fileContent = new ArrayList<String>();
            // fire line is title
            fileContent.add(req.getTitle());
            // rest of lines are requirement notes
            try { // url encoding to ensure special symbols/new line chars don't mess up storage file
                for (String n : req.getRequirementNotes()){
                    fileContent.add(URLEncoder.encode(n, "UTF-8"));
                }
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(CourseworkItem.class.getName()).log(Level.SEVERE, null, ex);
            }
            // write to a new requirement file
            try{
                writeTextFile(appDir + "/Courses/" + course + "/Coursework/requirement" + reqId + ".txt", fileContent);
            } catch (IOException ex){
                System.err.println("Error saving requirement to file.");
            }
            reqId++;
        }
    
    }
    
    
    protected void exportRequirements(){
        // store output lines before writing
        ArrayList<String> outputLines = new ArrayList<>();
        // for each requirement
        for (CourseworkRequirement req : requirements){
            // add title to file
            outputLines.add(req.getTitle());
            // for each requirement note
            for (String reqNote : req.getRequirementNotes()){
                // add note as dashed list to output
                outputLines.add("- " + reqNote);
            }
            // blank line between requirements
            outputLines.add("");
        }
        
        // write full output to out.txt file
        try{
            writeTextFile(appDir + "/Courses/" + course + "/Coursework.txt", outputLines);
        } catch (IOException ex){
            System.err.println("Error exporting coursework requirements to file.");
        }
    
    }
    
    
}
