package coursework;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.*;


public class Coursework extends JFrame implements ActionListener, KeyListener {
    
    private CommonCode cc = new CommonCode();

//    private JPanel pnl = new JPanel(new BorderLayout());
    private JTextArea txtNewNote = new JTextArea();
    private JTextArea txtDisplayNotes = new JTextArea();

    private JComboBox courseList = new JComboBox();
    private Boolean updatingCourseList = false;

    private JLabel courseTitle = new JLabel();

    private AllNotes allNotes = new AllNotes();
    private AllCourses allCourses = new AllCourses();

    private InputSentence inputSentence = new InputSentence();
    private SearchWord searchWord = new SearchWord();
    private BusiestDay busiestDay = new BusiestDay();
    private LargestCourse largestCourse = new LargestCourse();

    private JTextField search = new JTextField();


    public static void main(String[] args) {
        // This is required for the coursework
        // JOptionPane.showMessageDialog(null, "Lucas Bowers - lb0689s");
        Coursework prg = new Coursework();
    }

    // Using MVC
    public Coursework(){
        model();
        view();
        controller();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ("Close".equals(e.getActionCommand())){
            txtNewNote.setText("");

        }
        if ("Exit".equals(e.getActionCommand())){
            System.exit(0);
        }
        if ("NewNote".equals(e.getActionCommand())){
            addNote(txtNewNote.getText());
            txtNewNote.setText("");
        }
        if ("ChangeCourse".equals(e.getActionCommand())){
            if (!updatingCourseList){ // if dropdown not in process of being updated
                String newCourse;

                newCourse = (String) courseList.getSelectedItem();
                allCourses.setCourse(newCourse);
                //Update page title to show selected course
                updateCourseTitle();
                //Show all notes for new course
                addAllNotes();
            }
        }
        
        if ("AddCourse".equals(e.getActionCommand())){
            String newCourse;

            newCourse = JOptionPane.showInputDialog(null, "Please enter the title of the new Course...");
            // Entered course isn't blank
            if (!newCourse.equals("")){
                // add course to courses list
                allCourses.addCourse((newCourse));
                //Update the dropdown menu
                updateCourseList();
                
            }
        }
        if ("AmendCourse".equals(e.getActionCommand())){
            String target;
            String newTitle;

            //get new title
            target = allCourses.getCourse();
            newTitle = JOptionPane.showInputDialog(null, "Please enter the new title of course '" + target + "'");
            //set new title
            allCourses.amendCourse(target,newTitle);

            //update existing notes for this course
            for (Note n : allNotes.getAllNotes()){
                //if note is for original course
                if (target.equals(n.getCourse())){
                    //update notes course to new course title
                    n.setCourse(newTitle);
                }
            }

            //save notes to file
            allNotes.writeAllNotes();

            //update dropdown list
            updateCourseList();
        }
        if ("DeleteCourse".equals(e.getActionCommand())) {
            String delCourse = allCourses.getCourse();

            //if a course exists to be deleted
            if (!delCourse.equals("")) {
                int confirm = JOptionPane.showConfirmDialog(null,
                        "Are you sure you would like to delete this course and all associated notes?",
                        "alert",
                        JOptionPane.OK_CANCEL_OPTION);

                if (confirm == JOptionPane.YES_OPTION) {
                    // delete course
                    allCourses.deleteCourse(delCourse);
                    // update dropdown menu
                    updateCourseList();
                }
            }
        }
        if ("Coursework".equals(e.getActionCommand())){
            // open coursework editor screen
            allCourses.showCourseworkScreen();
//            CourseworkOverview cwScreen = new CourseworkOverview(new CourseworkItem(allCourses.getCourse()));
//            cwScreen.setVisible(true);
        }
        
        if("SearchSentence".equals(e.getActionCommand())){
            inputSentence.setSentence();

            ArrayList<String> sentence = inputSentence.getSentence();
            searchWord.searchForWord(sentence);
        }
        
        if("SearchKeyword".equals(e.getActionCommand())){
            String noteList = allNotes.searchAllNotesByKeyword("", search.getText(), allNotes.getNotesForCourse(allCourses.getCourse()));
            txtDisplayNotes.setText(noteList);
            
        }
        
        if ("BusiestDay".equals(e.getActionCommand())){
            String getBusiestDay = busiestDay.getBusiestDay(allNotes.getAllNotes());
            JOptionPane.showMessageDialog(null, getBusiestDay, "Busiest Day", JOptionPane.INFORMATION_MESSAGE);
            
        }
        
        if ("LargestCourse".equals(e.getActionCommand())){
            String getLargestCourse = largestCourse.getLargestCourse(allNotes.getAllNotes());
            JOptionPane.showMessageDialog(null, getLargestCourse, "Largest Course", JOptionPane.INFORMATION_MESSAGE);
            
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    private void model(){
        // add an example course if none currently added
        if (allCourses.getCourses().isEmpty()){
            //add example course
            allCourses.addCourse("ExampleCourse");
        }

    }

    private void view(){
        Font fnt = new Font("Georgia",Font.PLAIN,24);

        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setTitle("Coursework - Lucas Bowers");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // navbar
        JMenuBar menuBar = new JMenuBar();
        // 'Note' navbar dropdown
        JMenu note = new JMenu();
        // 'Course' navbar dropdown
        JMenu courseMenu = new JMenu();

        note = new JMenu("Note");
        note.setToolTipText("Note tasks");
        note.setFont(fnt);
        // add items to 'Note' dropdown
        note.add(makeMenuItem("New", "NewNote", "Create a new note.", fnt));
        note.addSeparator();
        note.add(makeMenuItem("Close", "Close", "Clear the current note", fnt));
        // add 'Note' dropdown to navbar
        menuBar.add(note);

        courseMenu = new JMenu("Course");
        courseMenu.setToolTipText("Course tasks");
        courseMenu.setFont(fnt);
        // add items to 'Course' dropdown
        courseMenu.add(makeMenuItem("Add Course", "AddCourse", "Add a new course", fnt));
        courseMenu.add(makeMenuItem("Amend Course", "AmendCourse", "Amend an existing course", fnt));
        courseMenu.add(makeMenuItem("Delete Course", "DeleteCourse", "Delete an existing course", fnt));
        courseMenu.addSeparator();
        courseMenu.add(makeMenuItem("Coursework", "Coursework", "Coursework Actions", fnt));
        // add 'course' dropdown to navbar
        menuBar.add(courseMenu);

        JMenu toolsMenu = new JMenu("Tools");
        toolsMenu.setToolTipText("Useful Tools");
        toolsMenu.setFont(fnt);
        // add items to 'Tools' dropdown
        toolsMenu.add(makeMenuItem("Search Sentence", "SearchSentence", "Search a sentence for a word", fnt));
        toolsMenu.add(makeMenuItem("Busiest Day", "BusiestDay", "Find the day on which most notes were made", fnt));
        toolsMenu.add(makeMenuItem("Largest Course", "LargestCourse", "Find the course with the most notes", fnt));
        // add 'Tools' dropdown to navbar
        menuBar.add(toolsMenu);

        // standalone menu items
        menuBar.add(makeMenuItem("Exit", "Exit", "Close this program", fnt));

        //blank menu item to stop 'exit' item growing to fill full width
        menuBar.add(makeMenuItem("","","",fnt));

        // add courses to JComboBox (courseList)
        // style JComboBox
        courseList.setFont(fnt);
        courseList.setMaximumSize(courseList.getPreferredSize());
        courseList.setActionCommand("ChangeCourse");
        courseList.addActionListener(this);
        // add Courses JCombo to menubar
        menuBar.add(courseList);

        //update course list to add all courses to dropdown
        updateCourseList();

        //set menubar as scenes menu bar
        this.setJMenuBar(menuBar);

        JToolBar toolBar = new JToolBar();
        // Setting up the button bar
        JButton button = null;
        // imageName, actionCmd, tooltipText, altText
        button = makeButton("Create", "NewNote", "Create a new note.", "New");
        toolBar.add(button);
        button = makeButton("closed door", "Close", "Close this note", "Close");
        toolBar.add(button);
        toolBar.addSeparator();
        button = makeButton("exit", "Exit", "Exit from this program", "Exit");
        toolBar.add(button);
        /*
        *
        * New Search way
        *
         */
        toolBar.addSeparator();
        //This forces anything after it to the right
        toolBar.add(Box.createHorizontalGlue());

        search.setMaximumSize(new Dimension(6900, 30));
        search.setFont(fnt);
        toolBar.add(search);
        toolBar.addSeparator();
        button = makeButton("search", "SearchKeyword",
                "Search for this text.",
                "Search");
        toolBar.add(button);

        add(toolBar, BorderLayout.NORTH);

        // create West Panel
        JPanel pnlWest = new JPanel();
        pnlWest.setLayout(new BoxLayout(pnlWest, BoxLayout.Y_AXIS));
        //pnlWest.setPreferredSize(new Dimension(400,400));
        pnlWest.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        // add txtNewNote to west panel
        txtNewNote.setFont(fnt);
        pnlWest.add(txtNewNote);

        // add 'Add note' button to West Panel
        JButton btnAddNote = new JButton("Add note");
        btnAddNote.setActionCommand("NewNote");
        btnAddNote.addActionListener(this);
        pnlWest.add(btnAddNote);

        // add west panel to scene
        add(pnlWest, BorderLayout.WEST);

        // create Center Panel
        JPanel cen = new JPanel();
        cen.setLayout(new BoxLayout(cen, BoxLayout.Y_AXIS));
        cen.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        //add course title to notes
        updateCourseTitle();
        Font fntTitle =new Font("Helvetica",Font.PLAIN,24);
        courseTitle.setFont(fntTitle);
        cen.add(courseTitle);

        //scrollable notes container in center panel to be added here
        txtDisplayNotes.setFont(fnt);
        cen.add(txtDisplayNotes);

        add(cen, BorderLayout.CENTER);

        setVisible(true);

    }

    private void controller(){
        addAllNotes();
    }

    /**
     * Update the courseList JComboBox
     * PUBLIC so it can be called from AllCourses class
     */
    public void updateCourseList(){
        // so "ChangeCourse" action event doesn't run
        updatingCourseList = true;
        
        // clear dropdown
        courseList.removeAllItems();
        if ((!allCourses.getCourses().isEmpty())){ // if atleast 1 course
            //add each course to dropdown
            for (String course : allCourses.getCourses()){
                courseList.addItem(course);
            }
            updatingCourseList = false;
        
            // set selected course to active course
            courseList.setSelectedItem(allCourses.getCourse());
        }
    }

    /**
     * update the center panel title with active courses
     */
    private void updateCourseTitle(){
        courseTitle.setText("<html><body><h2>Viewing notes for: <span style='font-size: 18px;'>" + allCourses.getCourse() + "</span></h2></body></html>");
    }

    protected JMenuItem makeMenuItem (
            String txt,
            String actionCommand,
            String toolTipText,
            Font fnt) {

        JMenuItem mnuItem = new JMenuItem();

        mnuItem.setText(txt);
        mnuItem.setActionCommand(actionCommand);
        mnuItem.setToolTipText(toolTipText);
        mnuItem.setFont(fnt);

        mnuItem.addActionListener(this);

        return mnuItem;
    }

    protected JButton makeButton(
            String imageName,
            String actionCommand,
            String toolTipText,
            String altText) {

        //Create and initialize the button
        JButton button = new JButton();
        button.setToolTipText(toolTipText);
        button.setActionCommand(actionCommand);
        button.addActionListener(this);

        //Look for the image
        String imgLocation = System.getProperty("user.dir")
                + "/icons/"
                + imageName
                + ".png";

        File fyle = new File(imgLocation);
        if (fyle.exists() && !fyle.isDirectory()) {
            //Image found
            Icon img;
            img = new ImageIcon(imgLocation);
            button.setIcon(img);
        } else {
            //image not found
            button.setText(altText);
            System.err.println("Resource not found: " + imgLocation);
        }

        return button;

    }

    private void addNote (String text){
        String course = allCourses.getCourse();
        if (course.equals("")){
            JOptionPane.showMessageDialog(null, "No course selected!", "Add Note", JOptionPane.ERROR_MESSAGE);
        }else{
            allNotes.addNote(course, text);

            //update output with new notes
            addAllNotes();
        }
    }

    private void addAllNotes(){
        txtDisplayNotes.removeAll();
        txtDisplayNotes.revalidate();
        txtDisplayNotes.repaint();

        ArrayList<Note> notesForCourse = allNotes.getNotesForCourse(allCourses.getCourse());

        String txtNote = "";

        for (Note n : notesForCourse){

            txtNote += n.getNote() + "\n";
        }

        txtDisplayNotes.setText(txtNote);
    }

    private String getDateAndTime(){
        String UK_DATE_FORMAT_NOW = "dd-MM-yyyy HH:mm:ss";
        String ukDateAndTime;
        Calendar cal = Calendar.getInstance();

        SimpleDateFormat uksdf = new SimpleDateFormat(UK_DATE_FORMAT_NOW);
        ukDateAndTime = uksdf.format(cal.getTime());

        return  ukDateAndTime;
    }
}
