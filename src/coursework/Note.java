package coursework;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Note extends CommonCode{
    private int noteID = 0;
    private String course = "";
    private String date = "";
    private String note = "";

    public Note(int id, String c, String d, String n){
        setNoteID(id);
        setCourse(c);
        setNote(n);

        // set date if none given
        if (d == null){
            setDate();
        }else{
            setDate(d);
        }
    }

    // Mutator methods (setters)
    public void setNoteID(int id){
        int newID = id;

        // validate note id here

        noteID = newID;
    }

    public void setCourse(String c){
        String newCourse = c;

        // validate course here

        course = newCourse;
    }

    public void setDate(){
        date = orderedDate;
    }

    public void setDate(String d){
        date = d;
    }

    public void setNote(String n){
        String noteTxt = n;

        // any validation goes here

        note = noteTxt;
    }


    // Accessor methods (getters)
    public int getNoteID(){
        // any checking goes here
        return noteID;
    }

    public String getCourse(){
        // any checking goes here
        return course;
    }

    public String getDate(){
        return date;
    }

    public String getNote(){
        // any checking goes here
        return note;
    }
}
