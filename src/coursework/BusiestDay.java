/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author lucasbowers
 */
public class BusiestDay {
    
    public BusiestDay(){}
    
    
    
    public String getBusiestDay(ArrayList<Note> notes){
        HashMap counts = new HashMap();
        String busiestDay = getBusiestDay(notes, counts, 0);
        
        return busiestDay;
    }
    
    public String getBusiestDay(ArrayList<Note> notes, HashMap counts, int counter){
        
        if (counter == notes.size()-1){
            String busiestDay = "";
            int highestCount = 0;
            
            Iterator it = counts.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                System.out.println(pair.getKey() + " = " + pair.getValue());
                it.remove(); // avoids a ConcurrentModificationException
                
                int thisCount = (int) pair.getValue();
                
                if ( thisCount > highestCount ){
                    busiestDay = (String) pair.getKey();
                    highestCount = thisCount;
                }
                
            }
            return busiestDay;

        }else{
            
            Note n = notes.get(counter);
            String dateTime = n.getDate();
            String[] tmp = dateTime.split(" ");
            String date = tmp[0];
            
            int newCount = 1;
            if (counts.containsKey(date)){
                newCount += ((int) counts.get(date));
            }
            counts.put(date, newCount);
            
            return getBusiestDay(notes, counts, counter+1);
            
        }
    }
    
}
